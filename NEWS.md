# Changelog for config-diag, the diagnostic output helper

## 1.0.2 (2022-09-01)

- Declare Rust edition 2021 with no changes.
- Honor some of clippy's suggestions:
  - use `.to_owned()` when converting a `&str` to a `String`
  - mark the exported functions as inline
  - add some keywords to the Cargo package metadata
  - drop the `Result` return type from a test function that does not
    use the `?` operator
  - localize a couple of variables in the test suite
  - explicitly note that some functions print to the standard output or
    the standard error streams
  - explicitly allow some lints in the test suite
- Add a `run-clippy.sh` tool to enable some code style checks.
- Keep the trivial `Cargo.lock` file under version control.

## 1.0.1 (2021-09-01)

- First public release.

Contact: [Peter Pentchev][roam]

[roam]: mailto:roam@ringlet.net
