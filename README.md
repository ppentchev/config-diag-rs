# config-diag -- provide a .diag() function for diagnostic output if requested

The `ConfigDiag` trait may be used to decorate an object that stores some
kind of configuration information. It adds a `diag` method that checks
whether verbose output has been requested and, if so, displays a message to
the standard error stream. If the `diag_to_stderr` method is overridden and
it returns false, any diagnostic messages selected for display are sent to
the standard output stream instead.

Example:

    use std::error;
    
    use config_diag::ConfigDiag;
    
    struct Config {
        verbose: bool,
    }
    
    impl ConfigDiag for Config {
        fn diag_is_verbose(&self) -> bool {
            self.verbose
        }
    }
    
    pub fn main() -> Result<(), Box<dyn error::Error>> {
        let config = Config { verbose: true };
        let event = 42;
        config.diag(|| format!("Something happened: {}", event));
        config.diag_("Got here!");
        Ok(())
    }
