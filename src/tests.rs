//! A couple of trivial tests for the [`ConfigDiag`] trait.

#![allow(clippy::default_numeric_fallback)]
#![allow(clippy::panic)]

use crate::ConfigDiag;

struct Config {
    value: i32,
}

impl ConfigDiag for Config {
    fn diag_is_verbose(&self) -> bool {
        self.value < 60
    }
}

#[test]
fn test_invoked_or_not() {
    let mut count = 0;

    {
        let config = Config { value: 42 };
        assert_eq!(count, 0);
        config.diag(|| {
            count += 1;
            format!("Value: {}", config.value)
        });
        assert_eq!(count, 1);
    }

    {
        let config = Config { value: 616 };
        assert_eq!(count, 1);
        config.diag(|| {
            count += 1;
            panic!("This should not be invoked");
        });
        assert_eq!(count, 1);
    }
}
